/*
* 	file : kmain.cpp
*
* 	This file is part of Lyra.
*
* 	Lyra is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Lyra is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Lyra.  If not, see <http://www.gnu.org/licenses/>.
*/

// Lyra
#include <lyra/kmain.hpp>
#include <lyra/kpanic.hpp>
#include <lyra/tty.hpp>
#include <lyra/kprompt.hpp>
#include <lyra/mempool.hpp>

namespace lyra
{
	// Kernel early
	extern "C" void kearly()
	{
		tty_init();
		tty_write_str("Started kernel TTY\n");

		mempool_init((ubyte*)0x1000000, 0x100000, 64); // At 16 MB, 1 MB in size, composed of blocks of 64 B
		tty_write_str("Initiated dynamic memory pool\n");

		tty_write_str("Finished early kernel boot\n");
	}

	// Kernel main
	extern "C" void kmain()
	{
		tty_write_str("Main kernel boot complete\n");

		tty_write_str("Welcome to Lyra OS\n");
		tty_write_str("Starting kernel prompt...\n\n");

		kprompt();
	}
}
