/*
* 	file : type.hpp
*
* 	This file is part of Lyra.
*
* 	Lyra is free software: you can redistribute it and/or modify
* 	it under the terms of the GNU General Public License as published by
* 	the Free Software Foundation, either version 3 of the License, or
* 	(at your option) any later version.
*
* 	Lyra is distributed in the hope that it will be useful,
* 	but WITHOUT ANY WARRANTY; without even the implied warranty of
* 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* 	GNU General Public License for more details.
*
* 	You should have received a copy of the GNU General Public License
* 	along with Lyra.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LYRA_TYPE_HPP
#define LYRA_TYPE_HPP

// GCC
#include <stddef.h>
#include <stdint.h>

namespace lyra
{
	typedef unsigned char ubyte;
	typedef signed char   sbyte;

	typedef unsigned char  uint8;
	typedef signed char    sint8;
	typedef unsigned short uint16;
	typedef signed short   sint16;
	typedef unsigned int   uint32;
	typedef signed int     sint32;
	typedef unsigned long  uint64;
	typedef signed long    sint64;

	typedef size_t umem;
}

#endif
