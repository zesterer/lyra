#!/bin/sh

DATE=`date --date="now" "+%Y-%m-%d"`

cp kernel/lyra.bin grub/isodir/boot/lyra.bin
cp grub/grub.cfg grub/isodir/boot/grub/grub.cfg
grub-mkrescue -o lyra.iso grub/isodir
#cp thoth.iso thoth-${DATE}.iso
